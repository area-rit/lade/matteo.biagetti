---
title: "High-redshift JWST Observations and Primordial Non-Gaussianity"
authors:
- admin
- Gabriele Franciolini
- Antonio Riotto

date: "2023-02-16T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "The Astrophysical Journal"
publication_short: "ApJ"

abstract: Several bright and massive galaxy candidates at high redshifts have been recently observed by the James Webb Space Telescope. Such early massive galaxies seem difficult to reconcile with standard Λ cold dark matter model predictions. We discuss under which circumstances such observed massive galaxy candidates can be explained by introducing primordial non-Gaussianity in the initial conditions of cosmological perturbations.
# Summary. An optional shortened abstract.
#summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.

tags:
  - EOS
  - N-body Simulations
  - Cosmology
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: https://iopscience.iop.org/article/10.3847/1538-4357/acb5ea
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  #caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

