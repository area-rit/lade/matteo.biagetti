---
title: The Covariance of Squeezed Bispectra
event: Cosmology Seminar
event_url: 

location: ETH Zurich
address:
  street: Rämistrasse 101
  city: Zurich
  region: 
  postcode: 8092
  country: Switzerland

summary: Strong cosmological constraints from galaxy surveys rely more and more on including higher-order correlation functions beyond the galaxy power spectrum. In this talk, we take important steps forward for a reliable power spectrum-bispectrum theoretical covariance to be employed in data analyses. We find a large correlation among (even mildly) squeezed halo bispectra. There is also a large correlation between these bispectra and the long-wavelength halo power spectrum. Thus, the commonly used diagonal Gaussian approximation fails to describe the covariance even approximately. These correlations have a large impact on parameter constraints from the bispectrum. We show this by measuring the covariance in a large set of N-body simulations, and comparing with a model that accounts for these correlations, keeping the leading non-Gaussian terms. The model is given in terms of the measured power spectrum and bispectrum, and therefore does not rely on perturbation theory. As an example, we study the Fisher information on the power spectrum and bispectrum of halos for primordial non-Gaussianity, which is particularly sensitive to squeezed bispectra. Using a simple Fisher matrix setting, constraints are degraded by a factor of ~2 using our realistic model with respect to the commonly used diagonal matrix.
# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: "2021-11-22T14:00:00Z"
#date_end: "2030-06-01T15:00:00Z"
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: "2017-01-01T00:00:00Z"

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

image:
#  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/bzdhc5b3Bxs)'
  focal_point: Right

links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
url_code: ""
url_pdf: ""
url_slides: "files/zurich.pdf"
url_video: ""

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

# Enable math on this page?
math: true
---

