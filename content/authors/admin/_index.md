---
# Display name
title: Matteo Biagetti

# Full name (for SEO)
first_name: Matteo
last_name: Biagetti

# Status emoji
# status:
#   icon: 

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Researcher

# Organizations/Affiliations to show in About widget
organizations:
  - name: AREA Science Park
    url: https://en.areasciencepark.it/rdplatform-2023/lade-laboratorio-di-data-engeneering/

# Short bio (displayed in user profile at end of posts)
bio: I am interested in the topological and geometric structure underlying high dimensional data from various datasets, ranging from cosmological datasets, to representations in neural network layers.

# Interests to show in About widget
interests:
- Topological Data Analysis
- Machine Learning
- Cosmology

# Education to show in About widget
education:
  courses:
  - course: PhD in Theoretical Physics
    institution: University of Geneva
    year: 2016
  - course: MSc in Theoretical Physics
    institution: University of Padova
    year: 2011
  - course: BSc in Physics
    institution: University of Padova
    year: 2009

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: '/#contact'
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/Matteo1Biagetti
  - icon: graduation-cap # Alternatively, use `google-scholar` icon from `ai` icon pack
    icon_pack: fas
    link: https://scholar.google.com/citations?user=gueOQiIAAAAJ&hl=en&oi=ao
  # - icon: github
  #   icon_pack: fab
  #   link: https://github.com/gcushen
  - icon: linkedin
    icon_pack: fab
    link: https://www.linkedin.com/in/matteo-biagetti-a8172458/?trk=hp-identity-name
  # Link to a PDF of your resume/CV.
  # To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.yaml`,
  # and uncomment the lines below.
  - icon: cv
    icon_pack: ai
    link: uploads/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: true
---

I am a research scientist. I have dedicated most of my academic career to the field of cosmology, which investigates the origin, evolution and ultimate fate of the Universe as a whole on its largest scales. Recently, I have been interested in innovative data-oriented techniques for analysing high-dimensional datasets, including cosmological ones. I have studied methods from topological data analysis, specifically persistent homology, and applied them to cosmological datasets.

I am currently employed as a Data Scientist at the Laboratory of Data Engineering ([LADE](https://en.areasciencepark.it/rdplatform-2023/lade-laboratorio-di-data-engeneering/)) at the Institute of Research and Technological Innovation, AREA Science Park in Trieste, Italy.
